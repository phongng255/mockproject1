import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PickListModule } from 'primeng/picklist';
import { TaskComponent } from './Task/Task.component';
import { FormsModule } from '@angular/forms';
import { FloatLabelModule } from 'primeng/floatlabel';
import { CalendarModule } from 'primeng/calendar';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ConfirmDialogModule } from 'primeng/confirmdialog';


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    PickListModule,
    FormsModule,
    FloatLabelModule,
    CalendarModule,
    InputSwitchModule,
    ButtonModule,
    InputTextModule,
    ConfirmDialogModule,
  ],
  declarations: [		
      TaskComponent,
      AppComponent,
   ],
  bootstrap: [AppComponent],
})
export class AppModule { }
