import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Task } from '../Model/Task';

@Component({
  selector: 'app-Task',
  templateUrl: './Task.component.html',
  styleUrls: ['./Task.component.css'],
})
export class TaskComponent implements OnInit {
  
  l_Submitted = false;
  l_model :Task = new Task('Task 1', 'Description of Task 1', new Date('2024-04-14'), 'Pending');
  l_Checked = false;
  @Output() messageEvent = new EventEmitter<Task>();
  
  ngOnInit() {
  }

  onSubmit() {
    this.l_Submitted = true;
    // console.log(this.l_model);
    this.messageEvent.emit(this.l_model);
  }
}
