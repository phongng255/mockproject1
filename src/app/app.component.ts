import { Component, DoCheck } from '@angular/core';
import { Task } from './Model/Task';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent{
  
  title = 'MockProject1';

  tasks: Task[] = [
    // { taskName: 'Task 1', description: 'Description of Task 1', dueDate: new Date('2024-04-14'), status: 'Pending' },
    // { taskName: 'Task 2', description: 'Description of Task 2', dueDate: new Date('2024-04-15'), status: 'Completed' },
    // { taskName: 'Task 3', description: 'Description of Task 3', dueDate: new Date('2024-04-16'), status: 'Pending' },
  ];

  targetTask: Task[] = [];

  receiveMessage($event: Task) {
    this.tasks.push($event);
    this.onPickListFocus();
  }

  onPickListFocus() {
    console.log("222222222");
  }
}