export  class  Task {
    taskName: string;
    description: string;
    dueDate: Date;
    status: string;
  
    constructor(taskName: string, description: string, dueDate: Date, status: string) {
      this.taskName = taskName;
      this.description = description;
      this.dueDate = dueDate;
      this.status = status;
    }
  }